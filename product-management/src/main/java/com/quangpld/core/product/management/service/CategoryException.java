package com.quangpld.core.product.management.service;

public class CategoryException extends Exception {

  /** serialVersionUID. */
  private static final long serialVersionUID = -5014790453429949006L;

  public enum Code {
    CATEGORY_NOT_FOUND_EXCEPTION;
  }

  /** Exception Code. */
  private final Code code;

  public CategoryException(Code code) {
    this.code = code;
  }

  public CategoryException(Code code, Throwable cause) {
    super(cause);
    this.code = code;
  }

  public CategoryException(Code code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }

  public CategoryException(Code code, String message) {
    super(message);
    this.code = code;
  }

  public Code getCode() {
    return code;
  }

}
