package com.quangpld.core.product.management.service;

import java.util.List;

import com.quangpld.core.product.management.entity.Category;

public interface CategoryService {

  public enum Status {
    ACTIVE,
    IN_ACTIVE;
  }

  /**
   * 
   * @param category
   * @throws Exception
   */
  void saveCategory(Category category) throws Exception;

  /**
   * 
   * @param category
   * @return
   * @throws Exception
   */
  Category updateCategory(Category category) throws Exception;

  /**
   * 
   * @param categoryId
   * @return
   * @throws Exception
   */
  Category findCategoryById(Integer categoryId) throws Exception;

  /**
   * 
   * @param category
   * @return
   * @throws Exception
   */
  List<Category> findCategoryByExample(Category category) throws Exception;

  /**
   * 
   * @param category
   * @throws Exception
   */
  void deleteCategory(Category category) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  int getCategoryCount() throws Exception;

  /**
   * 
   * @param categoryId
   * @throws Exception
   */
  void activeCategory(Integer categoryId) throws Exception;

  /**
   * 
   * @param categoryId
   * @throws Exception
   */
  void inActiveCategory(Integer categoryId) throws Exception;

}
