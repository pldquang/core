package com.quangpld.core.product.management.service;

import java.util.List;

import com.quangpld.core.product.management.entity.Product;
import com.quangpld.core.product.management.entity.ProductCost;

public interface ProductService {

  public enum Status {
    ACTIVE,
    IN_ACTIVE;
  }

  /**
   * 
   * @param product
   * @throws Exception
   */
  void saveProduct(Product product) throws Exception;

  /**
   * 
   * @param product
   * @return
   * @throws Exception
   */
  Product updateProduct(Product product) throws Exception;

  /**
   * 
   * @param productId
   * @return
   * @throws Exception
   */
  Product findProductById(Integer productId) throws Exception;

  /**
   * 
   * @param example
   * @return
   * @throws Exception
   */
  List<Product> findProductByExample(Product example) throws Exception;

  /**
   * 
   * @param product
   * @throws Exception
   */
  void deleteProduct(Product product) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  int getProductCount() throws Exception;

  /**
   * 
   * @param productId
   * @throws Exception
   */
  void activeProduct(Integer productId) throws Exception;

  /**
   * 
   * @param productId
   * @throws Exception
   */
  void inActiveProduct(Integer productId) throws Exception;

  /**
   * 
   * @param productCost
   * @throws Exception
   */
  void saveProductCost(ProductCost productCost) throws Exception;

  /**
   * 
   * @param productCost
   * @return
   * @throws Exception
   */
  ProductCost updateProductCost(ProductCost productCost) throws Exception;

  /**
   * 
   * @param productId
   * @return
   * @throws Exception
   */
  ProductCost getProductCost(Integer productId) throws Exception;

  /**
   * 
   * @param productId
   * @throws Exception
   */
  void deleteProductCost(Integer productId) throws Exception;

}
