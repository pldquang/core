package com.quangpld.core.product.management.service.impl;

import java.util.List;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.product.management.entity.Category;
import com.quangpld.core.product.management.service.CategoryException;
import com.quangpld.core.product.management.service.CategoryService;
import com.quangpld.core.product.management.service.CategoryException.Code;

public class CategoryServiceImpl implements CategoryService {

  /** Generic DAO for table CATEGORY. */
  private GenericDao<Category, Integer> categoryDao = null;

  /**
   * Default constructor.
   */
  public CategoryServiceImpl() {
  }

  /**
   * Setter for categoryDao.
   * 
   * @param categoryDao
   */
  public void setCategoryDao(GenericDao<Category, Integer> categoryDao) {
    this.categoryDao = categoryDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveCategory(Category category) throws Exception {
    categoryDao.saveOrUpdate(category);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Category updateCategory(Category category) throws Exception {
    categoryDao.saveOrUpdate(category);
    return category;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Category findCategoryById(Integer categoryId) throws Exception {
    return categoryDao.findById(categoryId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Category> findCategoryByExample(Category category) throws Exception {
    return categoryDao.findByExample(category);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteCategory(Category category) throws Exception {
    categoryDao.delete(category);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getCategoryCount() throws Exception {
    return (int) categoryDao.count();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void activeCategory(Integer categoryId) throws Exception {
    //
    Category got = categoryDao.findById(categoryId);
    if (got == null) {
      throw new CategoryException(Code.CATEGORY_NOT_FOUND_EXCEPTION);
    }

    //
    got.setStatus(Status.ACTIVE.toString());
    categoryDao.saveOrUpdate(got);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void inActiveCategory(Integer categoryId) throws Exception {
    //
    Category got = categoryDao.findById(categoryId);
    if (got == null) {
      throw new CategoryException(Code.CATEGORY_NOT_FOUND_EXCEPTION);
    }

    //
    got.setStatus(Status.IN_ACTIVE.toString());
    categoryDao.saveOrUpdate(got);
  }

}
