package com.quangpld.core.product.management.service.impl;

import java.util.List;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.product.management.entity.Product;
import com.quangpld.core.product.management.entity.ProductCost;
import com.quangpld.core.product.management.service.ProductException;
import com.quangpld.core.product.management.service.ProductException.Code;
import com.quangpld.core.product.management.service.ProductService;

public class ProductServiceImpl implements ProductService {

  /** Generic DAO for table PRODUCT. */
  private GenericDao<Product, Integer> productDao = null;

  /** Generic DAO for table PRODUCTCOST. */
  private GenericDao<ProductCost, Integer> productCostDao = null;

  /**
   * Default constructor.
   */
  public ProductServiceImpl() {
  }

  /**
   * Setter for productDao.
   * 
   * @param productDao
   */
  public void setProductDao(GenericDao<Product, Integer> productDao) {
    this.productDao = productDao;
  }

  /**
   * Setter for productCostDao.
   * 
   * @param productCostDao
   */
  public void setProductCostDao(GenericDao<ProductCost, Integer> productCostDao) {
    this.productCostDao = productCostDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveProduct(Product product) throws Exception {
    productDao.saveOrUpdate(product);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Product updateProduct(Product product) throws Exception {
    productDao.saveOrUpdate(product);
    return product;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Product findProductById(Integer productId) throws Exception {
    return productDao.findById(productId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Product> findProductByExample(Product example) throws Exception {
    return productDao.findByExample(example);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteProduct(Product product) throws Exception {
    try {
      deleteProductCost(product.getProductid());
    } catch (Exception e) {
      // TODO:
    }
    productDao.delete(product);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getProductCount() throws Exception {
    return (int) productDao.count();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void activeProduct(Integer productId) throws Exception {
    //
    Product got = productDao.findById(productId);
    if (got == null) {
      throw new ProductException(Code.PRODUCT_NOT_FOUND_EXCEPTION);
    }

    //
    got.setStatus(Status.ACTIVE.toString());
    productDao.saveOrUpdate(got);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void inActiveProduct(Integer productId) throws Exception {
    //
    Product got = productDao.findById(productId);
    if (got == null) {
      throw new ProductException(Code.PRODUCT_NOT_FOUND_EXCEPTION);
    }

    //
    got.setStatus(Status.IN_ACTIVE.toString());
    productDao.saveOrUpdate(got);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveProductCost(ProductCost productCost) throws Exception {
    productCostDao.saveOrUpdate(productCost);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ProductCost updateProductCost(ProductCost productCost) throws Exception {
    productCostDao.saveOrUpdate(productCost);
    return productCost;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ProductCost getProductCost(Integer productId) throws Exception {
    return productCostDao.findById(productId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteProductCost(Integer productId) throws Exception {
    //
    ProductCost got = productCostDao.findById(productId);
    if (got == null) {
      throw new ProductException(Code.PRODUCT_COST_NOT_FOUND_EXCEPTION);
    }

    //
    productCostDao.delete(got);
  }

}
