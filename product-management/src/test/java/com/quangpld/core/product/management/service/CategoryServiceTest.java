package com.quangpld.core.product.management.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.product.management.entity.Category;
import com.quangpld.core.product.management.service.CategoryService.Status;
import com.quangpld.core.product.management.util.ProductManagementUtils;

@RunWith(JUnit4.class)
public class CategoryServiceTest {

  /** CategoryService object. */
  private CategoryService categoryService = null;

  /** List of category to delete in tearDown method. */
  private List<Category> tearDownListCategories = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = ProductManagementUtils.getClassPathXmlApplicationContext();
    categoryService = context.getBean("categoryService", CategoryService.class);
    tearDownListCategories = new ArrayList<Category>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (Category category : tearDownListCategories) {
      categoryService.deleteCategory(category);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveCategory() throws Exception {
    //
    assertEquals(0, categoryService.getCategoryCount());
    int categoryId = createCategory("Electronics & Computers");
    assertEquals(1, categoryService.getCategoryCount());
    Category got = categoryService.findCategoryById(categoryId);
    assertNotNull(got);
    assertEquals("Electronics & Computers", got.getCategoryname());
    assertEquals(CategoryService.Status.ACTIVE.toString(), got.getStatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateCategory() throws Exception {
    //
    int categoryId = createCategory("Electronics & Computers");
    Category got = categoryService.findCategoryById(categoryId);
    assertNotNull(got);
    assertEquals("Electronics & Computers", got.getCategoryname());
    assertEquals(CategoryService.Status.ACTIVE.toString(), got.getStatus());
    assertNull(got.getCategorydescription());
    assertNull(got.getCategoryparent());

    //
    got.setCategoryname("Đồ điện tử");
    got.setCategorydescription("Lorem ipsum dolor sit amet");
    got.setStatus(CategoryService.Status.IN_ACTIVE.toString());
    Category updated = categoryService.updateCategory(got);
    assertNotNull(updated);
    assertEquals("Đồ điện tử", updated.getCategoryname());
    assertEquals(CategoryService.Status.IN_ACTIVE.toString(), updated.getStatus());
    assertEquals("Lorem ipsum dolor sit amet", updated.getCategorydescription());
    assertNull(updated.getCategoryparent());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindCategoryById() throws Exception {
    //
    int categoryId = createCategory("Electronics & Computers");
    Category got = categoryService.findCategoryById(categoryId);
    assertNotNull(got);
    assertEquals("Electronics & Computers", got.getCategoryname());
    assertEquals(CategoryService.Status.ACTIVE.toString(), got.getStatus());
    assertNull(got.getCategorydescription());
    assertNull(got.getCategoryparent());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindCategoryByExample() throws Exception {
    //
    createCategory("Electronics & Computers");
    createCategory("Clothing, Shoes & Jewelry");
    createCategory("Toys, Kids, Baby & Pets");

    //
    Category example = new Category();
    example.setCategoryname("Clothing, Shoes & Jewelry");
    List<Category> categories = categoryService.findCategoryByExample(example);
    assertEquals(1, categories.size());
    assertEquals("Clothing, Shoes & Jewelry", categories.get(0).getCategoryname());
    assertEquals(Status.ACTIVE.toString(), categories.get(0).getStatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteCategory() throws Exception {
    //
    assertEquals(0, categoryService.getCategoryCount());
    int categoryId = createCategory("Electronics & Computers");
    assertEquals(1, categoryService.getCategoryCount());
    Category got = categoryService.findCategoryById(categoryId);
    assertNotNull(got);
    assertEquals("Electronics & Computers", got.getCategoryname());
    assertEquals(CategoryService.Status.ACTIVE.toString(), got.getStatus());

    //
    categoryService.deleteCategory(got);
    tearDownListCategories.remove(0);
    assertEquals(0, categoryService.getCategoryCount());
    got = categoryService.findCategoryById(categoryId);
    assertNull(got);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetCategoryCount() throws Exception {
    //
    assertEquals(0, categoryService.getCategoryCount());
    createCategory("Electronics & Computers");
    createCategory("Clothing, Shoes & Jewelry");
    createCategory("Toys, Kids, Baby & Pets");
    assertEquals(3, categoryService.getCategoryCount());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testActiveCategory() throws Exception {
    //
    int categoryId = createCategory("Electronics & Computers");
    Category got = categoryService.findCategoryById(categoryId);
    got.setStatus(Status.IN_ACTIVE.toString());
    categoryService.updateCategory(got);
    got = categoryService.findCategoryById(categoryId);
    assertEquals(Status.IN_ACTIVE.toString(), got.getStatus());

    //
    categoryService.activeCategory(categoryId);
    got = categoryService.findCategoryById(categoryId);
    assertEquals(Status.ACTIVE.toString(), got.getStatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testInActiveCategory() throws Exception {
    //
    int categoryId = createCategory("Electronics & Computers");
    Category got = categoryService.findCategoryById(categoryId);
    assertEquals(Status.ACTIVE.toString(), got.getStatus());

    //
    categoryService.inActiveCategory(categoryId);
    got = categoryService.findCategoryById(categoryId);
    assertEquals(Status.IN_ACTIVE.toString(), got.getStatus());
  }

  private int createCategory(String categoryName) throws Exception {
    Category category = new Category(categoryName, CategoryService.Status.ACTIVE.toString());
    categoryService.saveCategory(category);
    tearDownListCategories.add(category);
    return category.getCategoryid();
  }

}
