package com.quangpld.core.product.management.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.product.management.entity.Category;
import com.quangpld.core.product.management.entity.Product;
import com.quangpld.core.product.management.entity.ProductCost;
import com.quangpld.core.product.management.service.ProductService.Status;
import com.quangpld.core.product.management.util.ProductManagementUtils;

@RunWith(JUnit4.class)
public class ProductServiceTest {

  /** CategoryService object. */
  private CategoryService categoryService = null;

  /** ProductService object. */
  private ProductService productService = null;

  /** List of category to delete in tearDown method. */
  private List<Category> tearDownListCategories = null;

  /** List of product to delete in tearDown method. */
  private List<Product> tearDownListProducts = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = ProductManagementUtils.getClassPathXmlApplicationContext();
    categoryService = context.getBean("categoryService", CategoryService.class);
    productService = context.getBean("productService", ProductService.class);
    tearDownListCategories = new ArrayList<Category>();
    tearDownListProducts = new ArrayList<Product>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
    for (Product product : tearDownListProducts) {
      productService.deleteProduct(product);
    }

    //
    for (Category category : tearDownListCategories) {
      categoryService.deleteCategory(category);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveProduct() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch", product.getProductname());
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
    Category category = categoryService.findCategoryById(product.getCategoryid());
    assertNotNull(category);
    assertEquals("Category 1", category.getCategoryname());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateProduct() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch", product.getProductname());
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
    Category category = categoryService.findCategoryById(product.getCategoryid());
    assertNotNull(category);
    assertEquals("Category 1", category.getCategoryname());

    //
    product.setProductname("Marc by Marc jacobs Amy Gold Watch");
    productService.updateProduct(product);
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals("Marc by Marc jacobs Amy Gold Watch", product.getProductname());
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
    category = categoryService.findCategoryById(product.getCategoryid());
    assertNotNull(category);
    assertEquals("Category 1", category.getCategoryname());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindProductById() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch", product.getProductname());
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
    Category category = categoryService.findCategoryById(product.getCategoryid());
    assertNotNull(category);
    assertEquals("Category 1", category.getCategoryname());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindProductByExample() throws Exception {
    //
    createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    createProduct("Marc by Marc jacobs Amy Gold Watch");
    Product example = new Product();
    example.setStatus(Status.ACTIVE.toString());
    List<Product> products = productService.findProductByExample(example);
    assertEquals(2, products.size());

    //
    example = new Product();
    example.setProductname("Marc by Marc jacobs Amy Gold Watch");
    products = productService.findProductByExample(example);
    assertEquals(1, products.size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteProduct() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch", product.getProductname());
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
    Category category = categoryService.findCategoryById(product.getCategoryid());
    assertNotNull(category);
    assertEquals("Category 1", category.getCategoryname());

    //
    productService.deleteProduct(product);
    tearDownListProducts.remove(0);
    assertEquals(0, productService.getProductCount());
    product = productService.findProductById(productId);
    assertNull(product);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetProductCount() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    createProduct("Marc by Marc jacobs MBM3056 Amy Gold Watch");
    createProduct("Michael Kors MK5676");
    assertEquals(3, productService.getProductCount());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testActiveProduct() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    product.setStatus(Status.IN_ACTIVE.toString());
    productService.updateProduct(product);

    //
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals(Status.IN_ACTIVE.toString(), product.getStatus());
    productService.activeProduct(productId);
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals(Status.ACTIVE.toString(), product.getStatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testInActiveProduct() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals(Status.ACTIVE.toString(), product.getStatus());

    //
    productService.inActiveProduct(productId);
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertEquals(Status.IN_ACTIVE.toString(), product.getStatus());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveProductCost() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertNull(productService.getProductCost(productId));

    //
    createProductCost(productId, 100.00d, "$");
    product = productService.findProductById(productId);
    assertNotNull(product);
    ProductCost productCost = productService.getProductCost(productId);
    assertNotNull(productCost);
    assertEquals(productId, productCost.getProductid());
    assertEquals(100.00d, productCost.getOriginalcost(), 0.00d);
    assertEquals("$", productCost.getCurrency());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateProductCost() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertNull(productService.getProductCost(productId));

    //
    createProductCost(productId, 100.00d, "$");
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertNotNull(productService.getProductCost(productId));
    ProductCost productCost = productService.getProductCost(productId);
    assertEquals(productId, productCost.getProductid());
    assertEquals(100.00d, productCost.getOriginalcost(), 0.00d);
    assertEquals("$", productCost.getCurrency());
    assertNull(productCost.getDiscountrate());
    assertNull(productCost.getDiscountvalue());

    //
    productCost.setDiscountrate(new Double(10.00d));
    productCost.setDiscountvalue(new Double(5.00d));
    productService.updateProductCost(productCost);
    productCost = productService.getProductCost(productId);
    assertEquals(productId, productCost.getProductid());
    assertEquals(100.00d, productCost.getOriginalcost(), 0.00d);
    assertEquals("$", productCost.getCurrency());
    assertNotNull(productCost.getDiscountrate());
    assertNotNull(productCost.getDiscountvalue());
    assertEquals(10.00d, productCost.getDiscountrate(), 0.00d);
    assertEquals(5.00d, productCost.getDiscountvalue(), 0.00d);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetProductCost() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertNull(productService.getProductCost(productId));

    //
    createProductCost(productId, 100.00d, "$");
    product = productService.findProductById(productId);
    assertNotNull(product);
    ProductCost productCost = productService.getProductCost(productId);
    assertNotNull(productCost);
    assertEquals(productId, productCost.getProductid());
    assertEquals(100.00d, productCost.getOriginalcost(), 0.00d);
    assertEquals("$", productCost.getCurrency());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteProductCost() throws Exception {
    //
    assertEquals(0, productService.getProductCount());
    int productId = createProduct("Invicta Women's 4718 II Collection Limited Edition Diamond Chronograph Watch");
    Product product = productService.findProductById(productId);
    assertNotNull(product);
    assertNull(productService.getProductCost(productId));

    //
    createProductCost(productId, 100.00d, "$");
    product = productService.findProductById(productId);
    assertNotNull(product);
    ProductCost productCost = productService.getProductCost(productId);
    assertNotNull(productCost);
    assertEquals(productId, productCost.getProductid());
    assertEquals(100.00d, productCost.getOriginalcost(), 0.00d);
    assertEquals("$", productCost.getCurrency());

    //
    productService.deleteProductCost(productId);
    product = productService.findProductById(productId);
    assertNotNull(product);
    assertNull(productService.getProductCost(productId));
  }

  private int createCategory(String categoryName) throws Exception {
    Category category = new Category(categoryName, CategoryService.Status.ACTIVE.toString());
    categoryService.saveCategory(category);
    tearDownListCategories.add(category);
    return category.getCategoryid();
  }

  private int createProduct(String productName) throws Exception {
    int categoryId = createCategory("Category 1");
    Product product = new Product(productName, Status.ACTIVE.toString());
    product.setCategoryid(categoryId);
    productService.saveProduct(product);
    tearDownListProducts.add(product);
    return product.getProductid();
  }

  private void createProductCost(int productId, double originalCost, String currency) throws Exception {
    ProductCost productCost = new ProductCost(productId, originalCost, currency);
    productService.saveProductCost(productCost);
  }

}
