package com.quangpld.core.parser;

import org.cyberneko.html.parsers.DOMParser;
import org.dom4j.Document;
import org.dom4j.io.DOMReader;

public class HtmlParser {

  public static Document parse(String url) throws Exception {
    //
    DOMParser parser = new DOMParser();
    parser.parse(url);

    //
    org.w3c.dom.Document document = parser.getDocument();
    DOMReader domReader = new DOMReader();  
    return domReader.read(document);
  }

}
