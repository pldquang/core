package com.quangpld.core.common.mail;

public interface MailSender {

  void sendMail() throws Exception;

}
