package com.quangpld.core.common.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class GmailTLSSender implements MailSender {

  private static final String MAIL_SMTP_AUTH_KEY = "mail.smtp.auth";
  private static final String MAIL_SMTP_AUTH_DEFAULT_VALUE = "true";
  private static final String MAIL_SMTP_STARTTLS_ENABLE_KEY = "mail.smtp.starttls.enable";
  private static final String MAIL_SMTP_STARTTLS_ENABLE_DEFAULT_VALUE = "true";
  private static final String MAIL_SMTP_HOST_KEY = "mail.smtp.host";
  private static final String MAIL_SMTP_HOST_DEFAULT_VALUE = "smtp.gmail.com";
  private static final String MAIL_SMTP_PORT_KEY = "mail.smtp.port";
  private static final String MAIL_SMTP_PORT_DEFAULT_VALUE = "587";

  private static final String UTF_8_ENCODING = "UTF-8";
  private static final String UTF_8_CONTENT_TYPE = "text/html; charset=utf-8";

  private static final String EMAIL_DEFAULT_DELIMITER = ",";

  private String username;
  private String password;
  private String fromMail;
  private String toMail;
  private String cc;
  private String bcc;
  private String mailSubject;
  private String mailContent;

  public GmailTLSSender(String username, String password, String fromMail, String toMail, String mailSubject, String mailContent) {
    this.username = username;
    this.password = password;
    this.fromMail = fromMail;
    this.toMail = toMail;
    this.mailSubject = mailSubject;
    this.mailContent = mailContent;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFromMail() {
    return fromMail;
  }

  public void setFromMail(String fromMail) {
    this.fromMail = fromMail;
  }

  public String getToMail() {
    return toMail;
  }

  public void setToMail(String toMail) {
    this.toMail = toMail;
  }

  public String getCc() {
    return cc;
  }

  public void setCc(String cc) {
    this.cc = cc;
  }

  public String getBcc() {
    return bcc;
  }

  public void setBcc(String bcc) {
    this.bcc = bcc;
  }

  public String getMailSubject() {
    return mailSubject;
  }

  public void setMailSubject(String mailSubject) {
    this.mailSubject = mailSubject;
  }

  public String getMailContent() {
    return mailContent;
  }

  public void setMailContent(String mailContent) {
    this.mailContent = mailContent;
  }

  @Override
  public void sendMail() throws Exception {
    //
    Properties props = new Properties();
    props.put(MAIL_SMTP_AUTH_KEY, MAIL_SMTP_AUTH_DEFAULT_VALUE);
    props.put(MAIL_SMTP_STARTTLS_ENABLE_KEY, MAIL_SMTP_STARTTLS_ENABLE_DEFAULT_VALUE);
    props.put(MAIL_SMTP_HOST_KEY, MAIL_SMTP_HOST_DEFAULT_VALUE);
    props.put(MAIL_SMTP_PORT_KEY, MAIL_SMTP_PORT_DEFAULT_VALUE);

    //
    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
      }
    });

    //
    List<String> ccLists = new ArrayList<String>();
    if (cc != null) {
      StringTokenizer st = new StringTokenizer(cc, EMAIL_DEFAULT_DELIMITER);
      while (st.hasMoreTokens()) {
        ccLists.add(st.nextToken());
      }
    }

    //
    List<String> bccLists = new ArrayList<String>();
    if (bcc != null) {
      StringTokenizer st = new StringTokenizer(bcc, EMAIL_DEFAULT_DELIMITER);
      while (st.hasMoreTokens()) {
        bccLists.add(st.nextToken());
      }
    }

    //
    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress(fromMail));
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));

    //
    for (String cc : ccLists) {
      message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
    }

    //
    for (String bcc : bccLists) {
      message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
    }

    //
    message.setSubject(mailSubject, UTF_8_ENCODING);
    message.setContent(mailContent, UTF_8_CONTENT_TYPE);
    Transport.send(message);
  }

}
