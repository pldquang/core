package com.quangpld.core.common.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HttpUtilsTest {

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetHostName() throws Exception {
    assertEquals("www.amazon.com", HttpUtils.getHostName("http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_1?ie=UTF8&qid=1359472046&sr=8-1&keywords=invicta"));
  }

}
