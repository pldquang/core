package com.quangpld.core.parser;

import static org.junit.Assert.assertEquals;

import org.dom4j.Document;
import org.dom4j.Node;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class HtmlParserTest {

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testParse() throws Exception {
    Document document = HtmlParser.parse("http://www.blueprintcss.org/tests/parts/sample.html");
    Node node = document.selectSingleNode("//DIV[@class='container']/H1");
    assertEquals("A simple sample page", node.getText());
//    
//    document = HtmlParser.parse("http://www.amazon.com/Invicta-Diver-Collection-Silver-Tone-Watch/dp/B0006AAS7E/ref=sr_1_1?ie=UTF8&qid=1359472046&sr=8-1&keywords=invicta");
//    node = document.selectSingleNode("//SPAN[@id='actualPriceValue']/B");
//    assertNotNull(node);
//    assertEquals("$54.99", node.getText());
  }

}
