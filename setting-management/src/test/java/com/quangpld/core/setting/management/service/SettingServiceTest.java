package com.quangpld.core.setting.management.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.util.SettingManagementUtils;

@RunWith(JUnit4.class)
public class SettingServiceTest {

  /** UserService object. */
  private SettingService settingService = null;

  /** List of users to delete in tearDown method. */
  private List<Setting> tearDownListSettings = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = SettingManagementUtils.getClassPathXmlApplicationContext();
    settingService = context.getBean("settingService", SettingService.class);
    tearDownListSettings = new ArrayList<Setting>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (Setting setting : tearDownListSettings) {
      settingService.deleteSetting(setting);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveSetting() throws Exception {
    //
    assertEquals(0, settingService.getSettingsCount());
    createSetting("test_key");
    assertEquals(1, settingService.getSettingsCount());
    Setting got = settingService.findSettingById("test_key");
    assertNotNull(got);
    assertEquals("test_key", got.getSettingkey());
    assertNull(got.getSettingvalue());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateSetting() throws Exception {
    //
    assertEquals(0, settingService.getSettingsCount());
    createSetting("test_key");
    assertEquals(1, settingService.getSettingsCount());
    Setting got = settingService.findSettingById("test_key");
    assertNotNull(got);
    assertEquals("test_key", got.getSettingkey());
    assertNull(got.getSettingvalue());

    //
    got.setSettingvalue("test_value");
    Setting updated = settingService.updateSetting(got);
    assertNotNull(updated);
    assertEquals("test_key", updated.getSettingkey());
    assertEquals("test_value", updated.getSettingvalue());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindSettingById() throws Exception {
    //
    assertEquals(0, settingService.getSettingsCount());
    createSetting("test_key");
    assertEquals(1, settingService.getSettingsCount());
    Setting got = settingService.findSettingById("test_key");
    assertNotNull(got);
    assertEquals("test_key", got.getSettingkey());
    assertNull(got.getSettingvalue());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteSetting() throws Exception {
    //
    assertEquals(0, settingService.getSettingsCount());
    createSetting("test_key");
    assertEquals(1, settingService.getSettingsCount());
    Setting got = settingService.findSettingById("test_key");
    assertNotNull(got);
    assertEquals("test_key", got.getSettingkey());
    assertNull(got.getSettingvalue());

    //
    settingService.deleteSetting(got);
    assertEquals(0, settingService.getSettingsCount());
    got = settingService.findSettingById("test_key");
    assertNull(got);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetSettingsCount() throws Exception {
    //
    assertEquals(0, settingService.getSettingsCount());
    createSetting("test_key_1");
    createSetting("test_key_2");
    createSetting("test_key_3");
    assertEquals(3, settingService.getSettingsCount());
  }

  private void createSetting(String settingkey) throws Exception {
    Setting setting = new Setting(settingkey);
    settingService.saveSetting(setting);
    tearDownListSettings.add(setting);
  }

}
