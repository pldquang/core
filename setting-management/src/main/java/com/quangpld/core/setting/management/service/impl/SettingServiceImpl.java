package com.quangpld.core.setting.management.service.impl;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.setting.management.entity.Setting;
import com.quangpld.core.setting.management.service.SettingService;

public class SettingServiceImpl implements SettingService {

  /** Generic DAO for table SETTING. */
  private GenericDao<Setting, String> settingDao = null;

  /**
   * Default constructor.
   */
  public SettingServiceImpl() {
  }

  /**
   * Setter for settingDao.
   * 
   * @param settingDao
   */
  public void setSettingDao(GenericDao<Setting, String> settingDao) {
    this.settingDao = settingDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveSetting(Setting setting) throws Exception {
    settingDao.saveOrUpdate(setting);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Setting updateSetting(Setting setting) throws Exception {
    settingDao.saveOrUpdate(setting);
    return setting;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Setting findSettingById(String key) throws Exception {
    try {
      return settingDao.findById(key);
    } catch (Exception e) {
      // TODO:
      return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteSetting(Setting setting) throws Exception {
    settingDao.delete(setting);
  }

  @Override
  public int getSettingsCount() throws Exception {
    return (int) settingDao.count();
  }

}
