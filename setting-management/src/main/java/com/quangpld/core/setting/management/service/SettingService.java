package com.quangpld.core.setting.management.service;

import com.quangpld.core.setting.management.entity.Setting;

public interface SettingService {

  /**
   * 
   * @param setting
   * @throws Exception
   */
  void saveSetting(Setting setting) throws Exception;

  /**
   * 
   * @param setting
   * @return
   * @throws Exception
   */
  Setting updateSetting(Setting setting) throws Exception;

  /**
   * 
   * @param key
   * @return
   * @throws Exception
   */
  Setting findSettingById(String key) throws Exception;

  /**
   * 
   * @param setting
   * @throws Exception
   */
  void deleteSetting(Setting setting) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  int getSettingsCount() throws Exception;

}
