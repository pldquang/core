/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-20
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-20    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.dao.core;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * HibernateGenericDao class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class HibernateGenericDao<T, Id extends Serializable> implements GenericDao<T, Id> {

  /** LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(HibernateGenericDao.class);

  /** clazz. */
  private Class clazz;

  /** Hibernate SessionFactory object. */
  private SessionFactory sessionFactory;

  /**
   * Constructor.
   * 
   * @param clazz
   */
  public HibernateGenericDao(Class clazz) {
    this.clazz = clazz;
  }

  /**
   * Setter for sessionFactory field.
   * 
   * @param sessionFactory
   */
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveOrUpdate(T entity) throws Exception {
    sessionFactory.getCurrentSession().saveOrUpdate(entity);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public T findById(Id id) throws Exception {
    return (T) sessionFactory.getCurrentSession().get(Class.forName(clazz.getName()), id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<T> findByExample(T exampleInstance) throws Exception {
    return sessionFactory.getCurrentSession().createCriteria(Class.forName(clazz.getName()))
                                             .add(Example.create(exampleInstance))
                                             .list();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<T> findByExample(T exampleInstance, int offset, int limit) throws Exception {
    return sessionFactory.getCurrentSession().createCriteria(Class.forName(clazz.getName()))
                                             .add(Example.create(exampleInstance))
                                             .setFirstResult(offset)
                                             .setMaxResults(limit)
                                             .list();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<T> findAll() throws Exception {
    return sessionFactory.getCurrentSession().createCriteria(Class.forName(clazz.getName())).list();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(T entity) throws Exception {
    sessionFactory.getCurrentSession().delete(entity);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long count() throws Exception {
    return (long) sessionFactory.getCurrentSession().createCriteria(Class.forName(clazz.getName())).list().size();
  }

}
