/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-20
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-20    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.dao.core;

import java.util.List;

/**
 * GenericDao interface.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public interface GenericDao<T, Id> {

  /**
   * 
   * @param entity
   * @throws Exception
   */
  void saveOrUpdate(T entity) throws Exception;

  /**
   * 
   * @param id
   * @return
   * @throws Exception
   */
  T findById(Id id) throws Exception;

  /**
   * 
   * @param exampleInstance
   * @return
   * @throws Exception
   */
  List<T> findByExample(T exampleInstance) throws Exception;

  /**
   * 
   * @param exampleInstance
   * @param offset
   * @param limit
   * @return
   * @throws Exception
   */
  List<T> findByExample(T exampleInstance, int offset, int limit) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  List<T> findAll() throws Exception;

  /**
   * 
   * @param entity
   * @throws Exception
   */
  void delete(T entity) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  long count() throws Exception;

}
