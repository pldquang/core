/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-02
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-02    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.user.management.entity.RolePermission;
import com.quangpld.core.user.management.service.RoleService;
import com.quangpld.core.user.management.service.UserException;
import com.quangpld.core.user.management.service.UserException.Code;

/**
 * RoleServiceImpl class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class RoleServiceImpl implements RoleService {

  /** Generic DAO for table ROLEPERMISSIONS. */
  private GenericDao<RolePermission, String> rolePermissionDao = null;

  /**
   * Default constructor.
   */
  public RoleServiceImpl() {
  }

  /**
   * Setter for rolePermissionDao.
   * 
   * @param rolePermissionDao
   */
  public void setRolePermissionDao(GenericDao<RolePermission, String> rolePermissionDao) {
    this.rolePermissionDao = rolePermissionDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> getRolePermission(String role) throws Exception {
    //
    RolePermission rolePermissions = rolePermissionDao.findById(role);
    if (rolePermissions == null) {
      return new ArrayList<String>();
    }

    //
    StringTokenizer st = new StringTokenizer(rolePermissions.getPermissions(), ",");
    List<String> permissions = new ArrayList<String>();
    while (st.hasMoreTokens()) {
      permissions.add(st.nextToken());
    }
    return permissions;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void grantRolePermission(String role, String permission) throws Exception {
    //
    RolePermission rolePermissions = rolePermissionDao.findById(role);
    if (rolePermissions == null) {
      rolePermissions = new RolePermission(role, permission);
      rolePermissionDao.saveOrUpdate(rolePermissions);
    } else {
      //
      StringTokenizer st = new StringTokenizer(rolePermissions.getPermissions(), ",");
      List<String> permissions = new ArrayList<String>();
      while (st.hasMoreTokens()) {
        permissions.add(st.nextToken());
      }

      //
      if (permissions.contains(permission)) {
        throw new UserException(Code.PERMISSION_ALREADY_EXIST);
      } else {
        permissions.add(permission);
        StringBuilder sb = new StringBuilder();
        for (String str : permissions) {
          sb.append(str);
          if (str != permissions.get(permissions.size() - 1)) {
            sb.append(",");
          }
        }
        rolePermissions.setPermissions(sb.toString());
        rolePermissionDao.saveOrUpdate(rolePermissions);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void revokeRolePermission(String role, String permission) throws Exception {
    //
    RolePermission rolePermissions = rolePermissionDao.findById(role);
    if (rolePermissions == null) {
      throw new UserException(Code.ROLE_NOT_FOUND);
    }

    //
    StringTokenizer st = new StringTokenizer(rolePermissions.getPermissions(), ",");
    List<String> permissions = new ArrayList<String>();
    while (st.hasMoreTokens()) {
      permissions.add(st.nextToken());
    }
    
    //
    if (!permissions.contains(permission)) {
      throw new UserException(Code.ROLE_HAS_NOT_GAINED_THIS_PERMISSION_YET);
    } else {
      permissions.remove(permission);
      StringBuilder sb = new StringBuilder();
      for (String str : permissions) {
        sb.append(str);
        if (str != permissions.get(permissions.size() - 1)) {
          sb.append(",");
        }
      }
      rolePermissions.setPermissions(sb.toString());
      rolePermissionDao.saveOrUpdate(rolePermissions);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void revokeAllRolePermissions(String role) throws Exception {
    //
    RolePermission rolePermissions = rolePermissionDao.findById(role);
    if (rolePermissions == null) {
      throw new UserException(Code.ROLE_HAS_NO_PERMISSION_YET);
    } else {
      rolePermissionDao.delete(rolePermissions);
    }
  }

}
