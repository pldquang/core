/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-03
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-03    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service;

/**
 * UserException class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class UserException extends Exception {

  /** serialVersionUID. */
  private static final long serialVersionUID = -2249039612317784102L;

  public enum Code {
    USER_ALREADY_EXIST,
    USER_NOT_FOUND,
    USER_HAS_NOT_GRANTED_THIS_ROLE_YET,
    USER_HAS_NO_ROLE_YET,
    ROLE_ALREADY_EXIST,
    ROLE_NOT_FOUND,
    ROLE_HAS_NOT_GAINED_THIS_PERMISSION_YET,
    ROLE_HAS_NO_PERMISSION_YET,
    PERMISSION_ALREADY_EXIST;
  }

  /** Exception Code. */
  private final Code code;

  /**
   * 
   * @param code
   */
  public UserException(Code code) {
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param cause
   */
  public UserException(Code code, Throwable cause) {
    super(cause);
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param message
   * @param cause
   */
  public UserException(Code code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param message
   */
  public UserException(Code code, String message) {
    super(message);
    this.code = code;
  }

  /**
   * Getter for code.
   * 
   * @return
   */
  public Code getCode() {
    return code;
  }

}
