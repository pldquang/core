/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-02-02
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-02-02    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service;

import java.util.List;

/**
 * RoleService interface.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public interface RoleService {

  /**
   * 
   * @param role
   * @return
   * @throws Exception
   */
  List<String> getRolePermission(String role) throws Exception;

  /**
   * 
   * @param role
   * @param permission
   * @throws Exception
   */
  void grantRolePermission(String role, String permission) throws Exception;

  /**
   * 
   * @param role
   * @param permission
   * @throws Exception
   */
  void revokeRolePermission(String role, String permission) throws Exception;

  /**
   * 
   * @param role
   * @throws Exception
   */
  void revokeAllRolePermissions(String role) throws Exception;

}
