/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2012-01-30
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-30    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.quangpld.core.dao.core.GenericDao;
import com.quangpld.core.user.management.entity.UserRole;
import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.service.UserException;
import com.quangpld.core.user.management.service.UserException.Code;
import com.quangpld.core.user.management.service.UserService;

/**
 * UserServiceImpl class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public class UserServiceImpl implements UserService {

  /** Generic DAO for table USER. */
  private GenericDao<User, String> userDao = null;

  /** Generic DAO for table USERROLE. */
  private GenericDao<UserRole, String> userRoleDao = null;

  /**
   * Default constructor.
   */
  public UserServiceImpl() {
  }

  /**
   * Setter for userDao.
   * 
   * @param userDao
   */
  public void setUserDao(GenericDao<User, String> userDao) {
    this.userDao = userDao;
  }

  /**
   * Setter for userRoleDao.
   * 
   * @param userRoleDao
   */
  public void setUserRoleDao(GenericDao<UserRole, String> userRoleDao) {
    this.userRoleDao = userRoleDao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveUser(User user) throws Exception {
    try {
      userDao.saveOrUpdate(user);
    } catch (Exception e) {
      throw new UserException(Code.USER_ALREADY_EXIST);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User updateUser(User user) throws Exception {
    userDao.saveOrUpdate(user);
    return user;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User findUserById(String username) throws Exception {
    return userDao.findById(username);
  }

  /**
   * {@inheritDoc}
   */
  public List<User> findUserByExample(User example) throws Exception {
    return userDao.findByExample(example);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteUser(User user) throws Exception {
    try {
      revokeAllUserRoles(user.getUsername());
    } catch (Exception e) {
      // TODO: Process Exception
    }
    userDao.delete(user);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hardDeleteUser(User user) throws Exception {
    // TODO: 
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getUsersCount() throws Exception {
    return (int) userDao.count();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> getUserRoles(String username) throws Exception {
    //
    User user = userDao.findById(username);
    if (user == null) {
      throw new UserException(Code.USER_NOT_FOUND);
    }

    //
    UserRole tblUserRoles = userRoleDao.findById(username);
    if (tblUserRoles == null) {
      return new ArrayList<String>();
    } else {
      List<String> roles = new ArrayList<String>();
      StringTokenizer st = new StringTokenizer(tblUserRoles.getRoles(), ",");
      while (st.hasMoreTokens()) {
        roles.add(st.nextToken());
      }
      return roles;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void grantUserRole(String username, String role) throws Exception {
    //
    User user = userDao.findById(username);
    if (user == null) {
      throw new UserException(Code.USER_NOT_FOUND);
    }

    //
    UserRole tblUserRoles = userRoleDao.findById(username);
    if (tblUserRoles == null) {
      tblUserRoles = new UserRole(username, role);
      userRoleDao.saveOrUpdate(tblUserRoles);
    } else {
      //
      StringTokenizer st = new StringTokenizer(tblUserRoles.getRoles(), ",");
      List<String> roles = new ArrayList<String>();
      while (st.hasMoreTokens()) {
        roles.add(st.nextToken());
      }
      
      //
      if (roles.contains(role)) {
        throw new UserException(Code.ROLE_ALREADY_EXIST);
      } else {
        roles.add(role);
        StringBuilder sb = new StringBuilder();
        for (String str : roles) {
          sb.append(str);
          if (str != roles.get(roles.size() - 1)) {
            sb.append(",");
          }
        }
        tblUserRoles.setRoles(sb.toString());
        userRoleDao.saveOrUpdate(tblUserRoles);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void revokeUserRole(String username, String role) throws Exception {
    //
    User user = userDao.findById(username);
    if (user == null) {
      throw new UserException(Code.USER_NOT_FOUND);
    }

    //
    UserRole userRole = userRoleDao.findById(username);
    if (userRole == null) {
      throw new UserException(Code.ROLE_NOT_FOUND);
    } else {
      //
      StringTokenizer st = new StringTokenizer(userRole.getRoles(), ",");
      List<String> roles = new ArrayList<String>();
      while (st.hasMoreTokens()) {
        roles.add(st.nextToken());
      }
      
      //
      if (!roles.contains(role)) {
        throw new UserException(Code.USER_HAS_NOT_GRANTED_THIS_ROLE_YET);
      } else {
        roles.remove(role);
        StringBuilder sb = new StringBuilder();
        for (String str : roles) {
          sb.append(str);
          if (str != roles.get(roles.size() - 1)) {
            sb.append(",");
          }
        }
        userRole.setRoles(sb.toString());
        userRoleDao.saveOrUpdate(userRole);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void revokeAllUserRoles(String username) throws Exception {
    //
    User user = userDao.findById(username);
    if (user == null) {
      throw new UserException(Code.USER_NOT_FOUND);
    }

    //
    UserRole userRole = userRoleDao.findById(username);
    if (userRole == null) {
      throw new UserException(Code.USER_HAS_NO_ROLE_YET);
    } else {
      userRoleDao.delete(userRole);
    }
  }

}
