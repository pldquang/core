/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2012-01-29
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-29    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service;

import java.util.List;

import com.quangpld.core.user.management.entity.User;

/**
 * UserService interface.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
public interface UserService {

  /**
   * 
   * @param user
   * @throws Exception
   */
  void saveUser(User user) throws Exception;

  /**
   * 
   * @param user
   * @return
   * @throws Exception
   */
  User updateUser(User user) throws Exception;

  /**
   * 
   * @param username
   * @return
   * @throws Exception
   */
  User findUserById(String username) throws Exception;

  /**
   * 
   * @param example
   * @return
   * @throws Exception
   */
  List<User> findUserByExample(User example) throws Exception;

  /**
   * 
   * @param user
   * @throws Exception
   */
  void deleteUser(User user) throws Exception;

  /**
   * 
   * @param user
   * @throws Exception
   */
  void hardDeleteUser(User user) throws Exception;

  /**
   * 
   * @return
   * @throws Exception
   */
  int getUsersCount() throws Exception;

  /**
   * 
   * @param username
   * @return
   * @throws Exception
   */
  List<String> getUserRoles(String username) throws Exception;

  /**
   * 
   * @param username
   * @param role
   * @throws Exception
   */
  void grantUserRole(String username, String role) throws Exception;

  /**
   * 
   * @param username
   * @param role
   * @throws Exception
   */
  void revokeUserRole(String username, String role) throws Exception;

  /**
   * 
   * @param username
   * @throws Exception
   */
  void revokeAllUserRoles(String username) throws Exception;

}
