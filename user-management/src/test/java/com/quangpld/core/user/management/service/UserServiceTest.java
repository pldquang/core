/*
 * Copyright 2013 (c) <quangpld>
 * 
 * Created on : 2013-01-31
 * Author     : quangpld
 * E-mail     : pldquang@gmail.com
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0-SNAPSHOT)
 *-----------------------------------------------------------------------------
 * VERSION             AUTHOR/         DESCRIPTION OF CHANGE
 * OLD/NEW             DATE            RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0.0-SNAPSHOT  | quangpld      | Initial Create.
 *                    | 2013-01-31    |
 *--------------------|---------------|----------------------------------------
 *                    | author        | 
 *                    | dd-mm-yy      | 
 *--------------------|---------------|----------------------------------------
 */
package com.quangpld.core.user.management.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;

import com.quangpld.core.user.management.entity.User;
import com.quangpld.core.user.management.util.UserManagementUtils;

/**
 * UserServiceTest class.
 * 
 * @author <a href="http://quangpld.com">pldquang@gmail.com</a>
 * @version 1.0.0-SNAPSHOT
 */
@RunWith(JUnit4.class)
public class UserServiceTest {

  /** UserService object. */
  private UserService userService = null;

  /** List of users to delete in tearDown method. */
  private List<User> tearDownListUsers = null;

  /**
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    ApplicationContext context = UserManagementUtils.getClassPathXmlApplicationContext();
    userService = context.getBean("userService", UserService.class);
    tearDownListUsers = new ArrayList<User>();
  }

  /**
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    for (User user : tearDownListUsers) {
      userService.deleteUser(user);
    }
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testSaveUser() throws Exception {
    //
    assertTrue(userService.getUsersCount() == 0);
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertTrue(userService.getUsersCount() == 1);
    User got = userService.findUserById("userabc");
    assertNotNull(got);
    assertEquals("123456", got.getPassword());
    assertEquals("userabc@yahoo.com", got.getEmail());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateUser() throws Exception {
    //
    assertTrue(userService.getUsersCount() == 0);
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertTrue(userService.getUsersCount() == 1);
    User got = userService.findUserById("userabc");
    assertNotNull(got);
    assertEquals("123456", got.getPassword());
    assertEquals("userabc@yahoo.com", got.getEmail());

    //
    got.setPassword("654321");
    got.setEmail("pldquang@gmail.com");
    User updatedUser = userService.updateUser(got);
    assertNotNull(updatedUser);
    assertEquals("654321", updatedUser.getPassword());
    assertEquals("pldquang@gmail.com", updatedUser.getEmail());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindUserById() throws Exception {
    //
    assertTrue(userService.getUsersCount() == 0);
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertTrue(userService.getUsersCount() == 1);
    User got = userService.findUserById("userabc");
    assertNotNull(got);
    assertEquals("123456", got.getPassword());
    assertEquals("userabc@yahoo.com", got.getEmail());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testFindUserByExample() throws Exception {
    //
    User example = new User();
    assertEquals(0, userService.findUserByExample(example).size());

    createUser("userabc1", "123456", "userabc1@yahoo.com");
    createUser("userabc2", "123456", "userabc2@yahoo.com");
    createUser("userabc3", "123456", "userabc3@yahoo.com");

    //
    assertEquals(3, userService.findUserByExample(example).size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteUser() throws Exception {
    //
    assertTrue(userService.getUsersCount() == 0);
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertTrue(userService.getUsersCount() == 1);
    User got = userService.findUserById("userabc");
    assertNotNull(got);

    //
    userService.deleteUser(got);
    tearDownListUsers.remove(0);
    assertNull(userService.findUserById("userabc"));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetUsersCount() throws Exception {
    //
    assertTrue(userService.getUsersCount() == 0);

    //
    for (int i = 0; i < 20; i++) {
      createUser("user" + i, "123456", "user" + i + "@gmail.com");
    }
    assertTrue(userService.getUsersCount() == 20);
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGetUserRoles() throws Exception {
    //
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertEquals(0, userService.getUserRoles("userabc").size());

    //
    userService.grantUserRole("userabc", "admin");
    assertEquals(1, userService.getUserRoles("userabc").size());
    assertEquals("admin", userService.getUserRoles("userabc").get(0));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testGrantRole() throws Exception {
    //
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertEquals(0, userService.getUserRoles("userabc").size());

    //
    userService.grantUserRole("userabc", "admin");
    assertEquals(1, userService.getUserRoles("userabc").size());
    assertEquals("admin", userService.getUserRoles("userabc").get(0));
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRevokeRole() throws Exception {
    //
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertEquals(0, userService.getUserRoles("userabc").size());

    //
    userService.grantUserRole("userabc", "admin");
    assertEquals(1, userService.getUserRoles("userabc").size());
    assertEquals("admin", userService.getUserRoles("userabc").get(0));

    //
    userService.revokeUserRole("userabc", "admin");
    assertEquals(0, userService.getUserRoles("userabc").size());
  }

  /**
   * 
   * @throws Exception
   */
  @Test
  public void testRevokeAllRoles() throws Exception {
    //
    createUser("userabc", "123456", "userabc@yahoo.com");
    assertEquals(0, userService.getUserRoles("userabc").size());

    //
    userService.grantUserRole("userabc", "admin");
    userService.grantUserRole("userabc", "moderator");
    assertEquals(2, userService.getUserRoles("userabc").size());
    assertEquals("admin", userService.getUserRoles("userabc").get(0));
    assertEquals("moderator", userService.getUserRoles("userabc").get(1));

    //
    userService.revokeAllUserRoles("userabc");
    assertEquals(0, userService.getUserRoles("userabc").size());
  }

  /**
   * 
   * @param username
   * @param password
   * @param email
   * @throws Exception
   */
  private void createUser(String username, String password, String email) throws Exception {
    User user = new User(username, password, email, Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok")).getTime());
    userService.saveUser(user);
    tearDownListUsers.add(user);
  }

}
